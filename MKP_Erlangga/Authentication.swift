//
//  Authentication.swift
//  MKP_Erlangga
//
//  Created by Erlangga Anugrah Arifin on 26/06/2024.
//

import UIKit

/// A class for managing user authentication made locally powered by UserDefaults.
class Authentication {
  
  /// Shared instance of the `Authentication` class. Use this to access authentication functionality.
  static let auth = Authentication()
  private init() {}
  
  /// Checks if a user is currently logged in by verifying if a username is stored in UserDefaults.
  /// - Parameter presentingViewController: The view controller from which the login screen should be presented if the user is not logged in.
  func checkLogin(presentingViewController: UIViewController) {
    if UserDefaults.standard.string(forKey: "username") == nil {
      let sb = UIStoryboard(name: "Main", bundle: nil)
      if let vc = sb.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
        vc.modalPresentationStyle = .fullScreen
        presentingViewController.present(vc, animated: true)
      }
    }
  }
  
  /// Removes the stored username and password from UserDefaults, effectively logging the user out.
  func removeLogin() {
    UserDefaults.standard.removeObject(forKey: "username")
    UserDefaults.standard.removeObject(forKey: "password")
  }
  
  /// Saves the provided username and password securely in UserDefaults for persistent login.
  /// - Parameters:
  ///   - username: The username of the user.
  ///   - password: The password of the user.
  func saveLogin(username: String, password: String) {
    let defaults = UserDefaults.standard
    defaults.set(username, forKey: "username")
    defaults.set(password, forKey: "password")
  }
}
