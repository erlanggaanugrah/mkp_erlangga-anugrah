//
//  LoginViewController.swift
//  MKP_Erlangga
//
//  Created by Erlangga Anugrah Arifin on 26/06/2024.
//

import UIKit

class LoginViewController: UIViewController {
  
  @IBOutlet weak var usernameTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var loginButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.passwordTextField.isSecureTextEntry = true
    usernameTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    passwordTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    loginButton.isEnabled = false
  }
  
  @objc func textFieldDidChange(_ textField: UITextField) {
    let isPasswordValid = passwordTextField.text?.count ?? 0 >= 8
    loginButton.isEnabled = usernameTextField.text?.count ?? 0 >= 1 && isPasswordValid
  }
  
  @IBAction func loginAction(_ sender: UIButton) {
    Authentication.auth.saveLogin(username: self.usernameTextField.text!, password: self.passwordTextField.text!)
    self.dismiss(animated: true)
  }
}
