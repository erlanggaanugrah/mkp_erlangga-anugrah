//
//  NFCModule.swift
//  MKP_Erlangga
//
//  MARK: Created & Documented by Erlangga Anugrah Arifin on 26/06/2024.
//

import CoreNFC

/// A class for reading NFC tags from prepaid cards. This class handles the process of establishing an NFC session, detecting tags, and extracting the unique identifier (UID) from ISO7816-compliant tags.
public class NFCTagReader: NSObject, NFCTagReaderSessionDelegate {
  
  /// A closure that is called when a valid UID is read from an NFC tag.
  /// - Parameter uidString: The UID of the tag in hexadecimal string format.
  public var onUIDRead: ((String) -> Void)?
  private var session: NFCTagReaderSession?
  
  /// Starts a new NFC reading session. This will prompt the user to hold their Prepaid Card near top the device.
  public func startReading() {
    session = NFCTagReaderSession(pollingOption: .iso14443, delegate: self)
    session?.alertMessage = "Tempelkan kartu pada area atas iPhone"
    session?.begin()
  }
  
  // MARK: NFCTagReaderSessionDelegate
  public func tagReaderSessionDidBecomeActive(_ session: NFCTagReaderSession) {
    // (Optional) Implement any UI updates or setup needed when the session becomes active.
    
  }
  
  /// Called when the NFC tag reader session is invalidated.
  /// - Parameter error: The error that caused the session to be invalidated.
  public func tagReaderSession(_ session: NFCTagReaderSession, didInvalidateWithError error: Error) {
    // (Optional) Handle any errors that occurred during the session.
  }
  
  /// Called when the NFC tag reader session detects one or more tags.
  /// - Parameters:
  ///   - session: The session that detected the tags.
  ///   - tags: An array of `NFCTag` objects representing the detected tags.
  public func tagReaderSession(_ session: NFCTagReaderSession, didDetect tags: [NFCTag]) {
    if case let .iso7816(iso7816Tag) = tags.last {
      let uidData = iso7816Tag.identifier // Filter for ISO7816 tags
      let uidString = uidData.map { String(format: "%02x", $0) }.joined()
      onUIDRead?(uidString) // Call the closure to pass the UID back
      session.invalidate() // End the session after reading
    }
  }
}
