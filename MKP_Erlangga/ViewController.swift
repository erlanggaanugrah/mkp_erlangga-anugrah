//
//  ViewController.swift
//  MKP_Erlangga
//
//  Created by Erlangga Anugrah Arifin on 26/06/2024.
//

import UIKit

class ViewController: UIViewController {
  
  @IBOutlet weak var hasilTextView: UILabel!
  let nfcErlangga = NFCTagReader()
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    Authentication.auth.checkLogin(presentingViewController: self)
  }
  
  @IBAction func logoutAction(_ sender: UIBarButtonItem) {
    Authentication.auth.removeLogin()
    Authentication.auth.checkLogin(presentingViewController: self)
  }
  
  @IBAction func tapTombol(_ sender: UIButton) {
    nfcErlangga.onUIDRead = { scanResult in
      DispatchQueue.main.async {
        self.hasilTextView.text = scanResult
      }
    }
    nfcErlangga.startReading()
  }
}
