
# Livin' by Mandiri e-Toll NFC UID Reader (iOS)

This iOS app allows users to read UID of their Livin' by Mandiri e-Toll prepaid cards using NFC technology.

## Development Information
- **Developed by:** Erlangga Anugrah
- **Development Target:** iPhone
- **Start Date:** June 26, 2024
- **Completion Date:** June 26, 2024
- **Purpose:** Submission for PT Mitra Kasih Perkasa


## Features

- NFC reading for Livin' by Mandiri e-Toll prepaid cards to get card's UID.

## Requirements

- Active Apple Developer Program account
- Xcode 11 or later
- iOS 13 or later
- iPhone 7 or later

## Installation

- Clone this project from GitLab or Xcode.

## Usage

- Run the app as usual. Tap your Livin' by Mandiri e-Toll card to the back of your iPhone to read card's UID.

## Technologies Used

- Programming Language: Swift
- UI Builder: UIKit
- NFC Reader: CoreNFC